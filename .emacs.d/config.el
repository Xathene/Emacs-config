
(defvar my-term-shell "/bin/bash")
(defadvice ansi-term (before force-bash)
  (interactive (list my-term-shell)))
(ad-activate 'ansi-term)

(defalias 'yes-or-no-p 'y-or-n-p)

(global-set-key (kbd "<s-return>")'ansi-term )

(use-package  which-key
  :ensure 
  :init
  (which-key-mode))

(use-package exwm
  :ensure t
  :config
  (require   'exwm-config)
  (exwm-config-default))

(use-package dmenu
  :ensure t
  :bind
(  "s-SPC"  . 'dmenu))

(use-package org-bullets
  :ensure t
  :config
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode))))

(use-package ido-vertical-mode
  :ensure t
  :init
  (ido-vertical-mode 1))
(setq ido-vertical0define-keys 'C-n-and-C-p-only)

(use-package smex
  :ensure t
  :init (smex-initialize)
  :bind
  ("M-x" . smex))

(use-package avy
  :ensure  t
  :bind
  ("M-s" . avy-goto-char))

(setq scroll-conservatively 100)

(global-hl-line-mode t)

(use-package beacon
  :ensure t
  :init
  (beacon-mode 1))

(setq make-backup-file nil)

(setq inhibit-startup-message t)

(defun exwm-async-run (program &rest args)
     "Asyncronously run program."
     (interactive (list (read-string "program: ")))
     (if (executable-find program)
         (make-process
          :name program
          :buffer nil
          :command (cons program args)
          :stderr nil)
       (message "Cannot run program `%s'" program)))

(setq ido-enable-flex-matching nil)
 (setq ido-create-new-buffer 'always)
 (setq ido-everywhere t)
 (ido-mode 1)

(global-set-key (kbd "C-x C-b") 'ibuffer)
